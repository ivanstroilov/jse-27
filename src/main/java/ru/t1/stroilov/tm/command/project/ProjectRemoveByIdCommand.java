package ru.t1.stroilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String DESCRIPTION = "Delete Project by ID.";

    @NotNull
    public final static String NAME = "project-delete-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECT BY ID]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectTaskService().removeProjectById(getUserId(), id);
    }
}
