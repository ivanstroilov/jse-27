package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Bind Task to Project.";

    @NotNull
    public final static String NAME = "task-bind-to-project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter Project ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(getUserId(), projectId, taskId);
    }
}
