package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.service.IProjectTaskService;
import ru.t1.stroilov.tm.api.service.ITaskService;
import ru.t1.stroilov.tm.command.AbstractCommand;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void showTaskList(@NotNull final List<Task> tasks) {
        @NotNull int index = 1;
        for (final Task task : tasks) {
            System.out.printf("%s[%s]. %s \n", index, task.getId(), task);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
