package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Complete Task by index.";

    @NotNull
    public final static String NAME = "task-complete-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(getUserId(), index, Status.COMPLETED);
    }
}
