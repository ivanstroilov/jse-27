package ru.t1.stroilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.model.User;

public class UserViewCommand extends AbstractUserCommand {

    @NotNull
    public final static String DESCRIPTION = "View User profile.";

    @NotNull
    public final static String NAME = "user-view";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
