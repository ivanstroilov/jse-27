package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Create a Task.";

    @NotNull
    public final static String NAME = "task-create";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        getTaskService().add(getUserId(), name, description);
    }
}
