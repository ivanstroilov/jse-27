package ru.t1.stroilov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @Getter
    @NotNull
    private final String displayName;

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static String toName(@Nullable final Status status) {
        if (status == null) return null;
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

}
