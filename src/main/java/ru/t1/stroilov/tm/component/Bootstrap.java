package ru.t1.stroilov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.repository.IUserRepository;
import ru.t1.stroilov.tm.api.service.*;
import ru.t1.stroilov.tm.command.AbstractCommand;
import ru.t1.stroilov.tm.command.data.AbstractDataCommand;
import ru.t1.stroilov.tm.command.data.DataLoadBase64Command;
import ru.t1.stroilov.tm.command.data.DataLoadBinaryCommand;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.exception.system.CommandNotSupportedException;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.model.Task;
import ru.t1.stroilov.tm.model.User;
import ru.t1.stroilov.tm.repository.CommandRepository;
import ru.t1.stroilov.tm.repository.ProjectRepository;
import ru.t1.stroilov.tm.repository.TaskRepository;
import ru.t1.stroilov.tm.repository.UserRepository;
import ru.t1.stroilov.tm.service.*;
import ru.t1.stroilov.tm.util.SystemUtil;
import ru.t1.stroilov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    @NotNull
    private final static String PACKAGE_COMMANDS = "ru.t1.stroilov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    public void run(@Nullable String... args) {
        if (processArgument(args)) processCommand("exit");
        prepareStartup();

        System.out.println("Please enter command: ");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                @Nullable final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                processCommand(command);
                System.out.println();
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        LOGGER_LIFECYCLE.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
    }

    private void prepareShutdown() {
        backup.stop();
        fileScanner.stop();
        LOGGER_LIFECYCLE.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        registry(clazz.newInstance());
    }

    private void initDemoData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        projectService.add(admin.getId(), new Project("Project1", Status.COMPLETED));
        projectService.add(admin.getId(), new Project("Project2", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("Project3", Status.NOT_STARTED));
        taskService.add(admin.getId(), new Task("Task1", "Task1"));
        taskService.add(admin.getId(), new Task("Task2", "Task2"));
    }

    public void processArgument(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @NotNull
    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, @NotNull final boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) {
            authService.checkRoles(abstractCommand.getRoles());
        }
        abstractCommand.execute();
    }

}