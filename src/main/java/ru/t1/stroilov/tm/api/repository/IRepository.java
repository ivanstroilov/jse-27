package ru.t1.stroilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void deleteAll();

    void deleteAll(@NotNull Collection<M> collection);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    boolean existsById(@NotNull String id);

    @Nullable
    M findById(@NotNull String id);

    @Nullable
    M findByIndex(@NotNull Integer index);

    @Nullable
    M delete(@Nullable M model);

    @Nullable
    M deleteById(@NotNull String id);

    @Nullable
    M deleteByIndex(@NotNull Integer index);

    @NotNull
    int getSize();

}
